---
title: "Social Media Components"
date: 2019-03-19T21:54:23-06:00
draft: true
---

To create media to distribute through the chosen social platforms, I've used a variety of tools. I utilized a camera to take photos for Instagram, designed icons and created an infographic in Figma, edited a short instructional video with an NLE, and listed/categorized content using Medium's simplistic formatting UI.

[Here you can read my Medium post about gear.](https://medium.com/@stellationrecordings/great-tools-for-a-diy-music-studio-in-2019-840e5ce3b6f9)

I added the five allowed tags to the Medium post, though refrained from adding hashtags to the Instagram posts, as I'm linking to a personal account and generally don't like to clutter posts I make there. However, I researched practices for optimal engagement on Instagram and found that there is currently a way to load-up a post with extra hashtags using a comment functionality workaround. Storing frequently used, categorized, trending hashtags in a remote text document helps speed up the process of content tagging on Instagram. Additionally, I’ve added a few SEO-focused meta tags to the html of this site. Pressing (Option+Command+U) should reveal a few of these optimizations towards the top of the markup of this page. Finally, I'm tracking parts of "lofrills.netlify.com" as a new property on Google Analytics.


A couple relevant instagram posts:
{{< instagram BLsWb1_lSV4 >}}
{{< instagram BcNt81kFsH- >}}