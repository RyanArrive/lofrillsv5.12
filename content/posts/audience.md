---
title: "The Audience Journey"
date: 2019-03-05T13:08:30-07:00
draft: true
featured_image: "/images/manWithBanjo.png"
---

My topic relates to music production in low budget contexts, emphasizing available techniques,
tools, and resources to construct musical recordings. I intend to target two main user categories;
current music producers seeking varied or new approaches, and beginners looking for materials
and information to get started. While the content will remain genre-adherent based on my
personal tastes (somewhat intrinsically), I would like to address an audience with varied
motivations and styles. Because of this, the content’s focus is primarily technical rather than
niche-centric or genre based.

![man With banjo](/images/manWithBanjo.png)

As the topic relates to low-budget production, I will assume a portion of the audience will only
access the content with a mobile device. Content will generally be created for mobile viewing,
including short-form articles, instructional videos, stills/photographs, lists, infographics, and
screencasts. As the content will live on various social networks, mobile viewports will be
addressed through these platform’s mobile experiences and native application environments.
The blog will amalgamate the disparate content into a single feed, viewable on both mobile and
desktop viewports. These content forms fall in the scope of guide & tutorial formats. As of now,
I don’t expect much user engagement. Without an established user-base it’s very hard to
anticipate use and retention. As the project progresses analytical tools will provide insight into
audience engagement.

### Hypothetical use-case/user persona:
Tonya has played piano for much of her life. She wants to record some of the songs she’s learned
so she can send them to family members and post them on sharing platforms. Additionally, she’s
composed a few arrangements which she would like to record. However, when she utilizes the
default recording application on her phone, she doesn’t like the results and wishes the overall
quality could be enhanced. Curious what she can do with minimal materials and knowledge, she
turns to some of her go-to social media networks for information.

{{< youtube hAB2a2Ud5ZM >}}