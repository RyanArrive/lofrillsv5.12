---
title: "Networks, Content Types and Legal Use"
date: 2019-03-05T13:00:07-07:00
draft: true
---

### Archiving: 
Archiving, as a social media term, broadly relates to the storage of media records,
specifically in business contexts where “paper-trails” and records are paramount for a variety of
reasons. To me, it seems archiving can relate to users beyond the business context. Downloading
a copy of your personal Facebook data, for instance, is an act of archiving. This process is very
important because it frames content through the lens of information control.
### Dark Social: 
Dark social refers to social sharing wherein analytics cannot be utilized, when a
web referral is difficult or impossible to track. Popular social media gives developers, brands,
etc., the tools to easily track the way their content spreads. It’s important to understand that many
users - though verifiably not the majority - access media in ways other than popularized
networks.
### Marketing automation: 
Marketing automation refers to a tactical system of digital delivery
catered to individualized content requests based on a user’s position within the funnel. These
systems, which essentially automate the process of targeted marketing, are comprised of a
variety of technologies and frameworks.
### Sentiment analysis:
 A sentiment analysis is a process of attempting to understand general trends
of an audience’s feelings about a company, brand, or products through computational means. For
instance, a software may be deployed to parse an array of text-based comments on a social media
post. The software will programmatically search for positive and negative keywords, phrases,
and indications of sentiment among the array. Sentiment analysis can be simplified into
personalized analysis. Perusing comments on your own personal account, for instance, is an act
of sentiment analysis. This approach becomes less effective as the brands size increases, or as
content becomes more varied and prolific.
### SoLoMo:
 SoLoMo is a way of describing three large trends in the way users consume
information in our age; social media, location based relevance, and mobile use. This term is
important to know because it has high relevance to the way most people consume media today.
### Vanity URL:
 A vanity URL is a short, memorable URL. Sometimes, the top-level domain
completes a word or acronym for the owner. For example: “ti.me” or “cheer.io.” A vanity URL
is acquired for its simplicity and aesthetic value pertaining to branding. When purchasing/trading
domains, it’s important to recognize the importance of vanity URLs.
### Networks
I will create content for Mastodon, Medium, YouTube, Reddit, Instagram. I have little
experience with Mastodon, but it seems like a great, open-source alternative to traditional forms
of social media. Mastodon is niche-based, and follows Reddit’s model of enclosed, interestbased communities. With mastodon, these communities are referred to as instances, and can be
created by anyone willing to host an instance. Many developers I follow include Mastodon links
on their sites, which I take as a sign the platform is moving in the right direction. The posts
residing within an instance on a Mastodon feed are short, so the content I create for Mastodon 
will direct users towards other channels while providing value. Reddit, like mastodon, is centered
around the idea of an individualized feed. Depending on the chosen topic, Reddit can provide a
variety of communities where niche content can be posted. I enjoy Medium as a platform but
I’ve only used it to gather information rather than create it. It has some nice topic-based
recommendations, a useful tool to get more eyes on relevant posts. YouTube is the most popular
video host/network in the Unites States, likely the world. Finally, I’ve included Instagram as a
sharing platform because of its high popularity. There is high demand for content production for
Instagram.
### Content Types
The media types I would like to work with in this class include short-form articles, instructional
videos, stills/photographs, lists, infographics, and screencasts. These media forms are ubiquitous
in the digital landscape and are important communication tools. Short-form articles usually relate
to industry developments and news. Based on my chosen topic, this media form will allow
timely, current, relevant content creation. Video is a highly popular media form which, when
executed correctly, can easily maintain an audience’s attention. Still & photos maintain
relevancy, especially on platforms such as Instagram. Lists are digestible and easy to produce,
which make them a highly-sought media form. Infographics portray information in a pleasing
way and oftentimes help us understand data in ways we couldn’t without the visualization.
Finally, screencasts will be included to communicate computer workflows and systematic
information. Technical processes can be easily understood utilizing screencasts.
### Legal Use
Many of the legal issues presented in module one relate to unoriginal content in production
contexts. As noted, it’s very easy to infringe on creative rights if you are unsure of the source of
the content you are posting, or do not understand the way it is licensed. Furthermore, there are a
slew of formalities attached to the definition of “fair use” that are very hard to navigate. I intend
to create highly authentic, original content in this class. When I need to “recycle” material for
clarity, I will make it abundantly clear where the material was found, providing proper credit the
source. If the material is not legally available for use, or if I am unsure of its standing in any
capacity, I will find different means.