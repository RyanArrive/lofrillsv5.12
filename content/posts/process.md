---
title: "How I made this site"
date: 2019-03-05T19:54:24-07:00
draft: true
---

To create this site I've used Hugo, a fast and flexible static site generator
built with love by spf13 and friends in Go. Unfortunately, I don't have much experience with this framework, so please excuse any weird formatting, glitches, etc. Additionally, I didn't realize that Netlify requires a custom domain to issue SSL cert, so this entire site may end up under a subdomain on a domain I already own. Side note: looks like lofrills.com is available, so I may snag it arbitrarily. Hugo is a binary which, when run, essentially scaffolds a website for you. To install I used my package manager of choice on OSX, Homebrew. Hugo versions can also be acquired trough NPM. To host, the private repository for this site is living on GitLab. I'm utilizing a service called Netlify to deploy.