---
title: "Final Production"
date: 2019-05-12T23:41:24-06:00
draft: true
featured_image: "/images/header2.png"
---

## I Designed a Logo
LoFrills is about the constructive, technical side of musical production:
![LoFrills logo](/images/header2.png)

I found that a lot of the content I’ve produced for this blog related to the production of the blog itself. For this reason, I felt like it was necessary to address the technical aspects of my project within the design while adhering to my initial topic objective pertaining to musical production.

Luckily, I think the design works both ways. The logo, drafted-up in Figma, is meant to illustrate my deviance into the consuming whirlpool that Hugo and overall JAMstack development turned out to be. I definitely became overly involved in the code and technological side of my project, which distracted from my ability to post as frequently as I may have otherwise throughout the semester. This being said, I picked-up a lot along the way, learning the basics of Git, Hugo, and the Instagram API. Additionally, I figured out ways to integrate social media content into my website in a visually appealing, functionally sound way. I think using these integrations in the future will be very helpful in my media pursuits. 

## Ananke Hugo: Easy GoogleAnalytics Integration
I didn't get around to posting my analytical integrations, unfortuantely. Here's an overview, excluding dashboard UI's provided by social media platforms (tracking codes in YouTube, for instance, can be integrated with Google Analytics). This is a screenshot from VS Code showing the GA tracking code entry on Ananke's 'congif.toml.' 

![analytics](/images/final/analytics.png)

The screenshot below shows a useful data visualization provided on GA’s UI. It illustrates acquisition sources from three primary categories. As you can see, referrals from the social platforms I’ve used comprise 57.1% of my site’s discernable traffic. 

![analytics](/images/final/acquisition.png)

I forgot to target my Reddit referrals specifically, but I suspect that they comprise the majority. There are many micro-communities on Reddit pertaining to my topic of LoFi production. Occasionally I’d direct them towards my content, especially if they were actively seeking advice. Here’s an example:

![reddit](/images/final/inquiry.PNG)
A response directs towards a medium article I'd posted about gear:
![reddit](/images/final/response.PNG)
<h3><a href="https://medium.com/@LoFrills/great-tools-for-a-diy-music-studio-in-2019-840e5ce3b6f9">Here's that article</a></h3>
## My Favorite Hugo Features
Hugo, build in the Go programming language, includes as built-in sever for live testing. This gives it instant responsivity to saved changes within your IDE, and sort of feels like a CMS in that way, especially if you use split windows on your display. Here’ a picture of the assuring terminal output Hugo renders upon spinning-up a server with ‘drafts’ enabled. 

![hugoServer](/images/final/hugoServer.png)

Finally, the directory that Hugo builds is entirely customizable. Themes are alterable down to every line of code, ensuring that your content remains malleable to any need you may have as a digital publisher. Working in this system has made me determined to build my own theme using Go’s templating. Hugo is highly extensible, especially when used in conjunction with other services surrounding the JAMstck, such as the Netlify CMS (headless). Even the WordPress CMS can be utilized through the rest API. Here’s one last screenshot showing how social partials can be customized to include current and future social media platforms entering the market: 

![hugoPartials](/images/final/socialPartials.png)

Thanks for reading! Don't hesitate to reach out: 

ryanmattmcdonald@gmail.com

@RyanArrive (everywhere)
