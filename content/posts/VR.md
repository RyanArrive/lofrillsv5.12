---
title: "Virtual Reality and the Futurist"
date: 2019-04-22T15:22:37-06:00
draft: true
---

### Virtual Reality and Futurist

People engage in their communities by sharing what they care about. Traditionally, this has taken the form of text, photos, and videos but today we are experiencing the propagation of more immersive media types. About a year ago, Facebook launched support for 3D objects utilizing thçe glTF 2.0 industry standard, a protocol I believe will continue to rise in popularity as VR develops. This adoption shows that our largest social platforms are migrating towards a virtual experience that includes literal depth. 

The burgeoning ecosystem of 3D content is one of the most exciting trends in the technology space. At the convergence of web development and art the metaverse will exhibit its greatest advantages. As futurists search for new developments, sharing platforms, and technical viability, especially in social contexts, the demand will fuel innovation. Communities built entirely upon VR already exist. 

The tools to create our own virtual experiences are being abstracted. We are working towards a digital world where people can create and share their own visions for immersive experiences. For example, a user can create 3D models and display them in-browser using open-source, free, well-supported tooling, such as the Blender software and the A-Frame VR framework. With a little more work, this process is infinitely extensible, allowing users to create their own online worlds. 

VR applications exist for music production. The boundaries of audio experimentation are pushed as artists are supplied with new virtual tools. Additionally, audio visualization has never been so rich. A production space can be iterated to fit the preferred environment of the user, their workflow, sound aesthetic, and preferences. 

I made the video below to quickly show how easy it is to create a VR experience using basic web tooling. In the context of music production, a model could play a particular sound on mouseover or click events. For instance, the keys of a blender-modeled cyberpunk keyboard could be mapped to an array of WAV files. 

{{< youtube eqiH5_CZDO0 >}}